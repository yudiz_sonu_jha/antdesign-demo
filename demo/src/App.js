import { Card } from 'antd';
import { Carousel } from 'antd';



import { Button, DatePicker, version } from "antd";


import './App.css';
// {}
import { Layout } from 'antd';

const { Header } = Layout;

const { Meta } = Card;



const contentStyle = {
  height: '160px',
  color: '#fff',
  lineHeight: '160px',
  textAlign: 'center',
  background: '#364d79',
};

function App() {
  return (
    <div className="App">
        <Layout>
          <Header style={{color:"white" ,fontSize:"30px",fontFamily:"italic"}}>For Developers </Header>
          <div style={{display:"flex"}}>
                <Card
              hoverable
              style={{ width: 240,margin:"50px"}}
              cover={<img alt="example" src="https://www.masaischool.com/img/courses/data.svg" />}
            >
              <Meta title="Data Analytics"  />
        </Card>,

        <Card
              hoverable
              style={{ width: 240,margin:"50px" }}
              cover={<img alt="example" src="https://www.masaischool.com/img/courses/web.svg" />}
            >
              <Meta title="FUll Stack Development Course" />
        </Card>,

        <Card
              hoverable
              style={{ width: 240,margin:"50px" }}
              cover={<img alt="example" src="https://www.masaischool.com/img/courses/android.svg" />}
            >
              <Meta title="FUll Stack Android Development" />
        </Card>,

         <Card
              hoverable
              style={{ width: 240,margin:"50px" }}
              cover={<img alt="example" src="https://www.masaischool.com/img/courses/android.svg" />}
            >
              <Meta title="FUll Stack Android Development"/>
        </Card>,

            
          </div>

              <p style={{fontSize:"30px",fontFamily:"italic"}}>What Companies Are Saying About  Developer</p>


           <div  style={{display:"flex",marginLeft:"110px"}}>
               <Card
                  hoverable
                  style={{ width: 340,marginLeft:"10px",height:'300px',borderRadius:"30px" }}
                  cover={<img alt="example" src="https://masai-website-images.s3.ap-south-1.amazonaws.com/Anirban_Majumdar_e001629fcc.jpeg" style={{ width: 60,height:"50px",margin:"20px",borderEndEndRadius:"20px"}}/>}
                >
                  <p style={{marginTop:"-80px"}}>Co-Founder -Urban Piper</p>

                  <p>There are many platforms that bolster an individual's "knowledge" to show up to work and just do their tasks. But with Masai, it is quite evident that the individuals are given a well-rounded education into the different aspects besides programming that go into software engineering.  programming hasn't really evolved to keep  practical aspects of software engineering.</p>


                  <Meta title="" description="" />
              </Card>,

               <Card
                  hoverable
                  style={{ width: 340,marginLeft:"310px",height:'300px' ,borderRadius:"30px" }}
                  cover={<img alt="example" src="https://masai-website-images.s3.ap-south-1.amazonaws.com/Arya_Adarsha_Gautam_a72b51d7b3.jpeg" style={{ width: 60,height:"50px",margin:"20px",borderEndEndRadius:"20px"}}/>}
                >
                  <p style={{marginTop:"-80px"}}>Co-Founder -Urban Piper</p>

                  <p>Masai does a great job of attracting ambitious folk and it shows in the hunger with which the grads chase their own improvement and career advancement. The curriculum is shaped such that its graduates come into the job with with an understanding of workplace and technology practices that is simply missing from freshers hired from colleges.</p>


                  <Meta title="" description="" />
              </Card>,

              <div>
                <h1>Date</h1>
                    <DatePicker />
                    <Button type="primary" style={{ marginLeft: 8 }}>
                      Primary Button
                    </Button>
                </div>


           </div>
        
                <Carousel autoplay style={{height:"100px",width:"50%",margin:"auto",marginTop:"30px"}}>
                  <div style={{display:"flex",margin:"auto"}}>
                    <h2 style={{display:"flex",marginLeft:"50px"}}>Carousel </h2>
                    <img style={{height:"140px",width:"50%",display:"flex"}} 
                    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQlcAMtM2iA-dVsVvQ9pvjk_f_lpUWa6b1TJg&usqp=CAU"/>                  
                   
                  </div>
                  <div style={{display:"flex",margin:"auto"}}>
                    <h2 style={{display:"flex",marginLeft:"30px"}}>Carousel </h2>
                   <img style={{height:"140px",width:"50%",display:"flex"}} 
                    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSpLzMewm9M3ac5D7CwpyDkmTiVzt-m282t1A&usqp=CAU"/>                  
                   
                  </div>
                  <div style={{display:"flex",margin:"auto"}}>
                    <h2 style={{display:"flex",marginLeft:"30px"}}>Carousel </h2>
                   <img style={{height:"140px",width:"50%",display:"flex"}} 
                    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTlst33n37g93m8liDygxtpqoPise6krFHX8g&usqp=CAU"/>                  
                   
                  </div>
                  <div style={{display:"flex",margin:"auto"}}>
                    <h2 style={{display:"flex",marginLeft:"30px"}}>Carousel </h2>
                    <img style={{height:"140px",width:"50%",display:"flex"}} 
                    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShvou5BjephjU9utwrVKaKPDfLbClhQsPYgw&usqp=CAU"/>                  
                   
                  </div>
                </Carousel>              

        </Layout>

    </div>
  );
}

export default App;
