
import './App.css';
// {}
import { Layout, Menu ,Breadcrumb } from 'antd';
import { Typography } from 'antd';
import SubMenu from 'antd/lib/menu/SubMenu';

const { Title } = Typography;
const { Header, Footer, Sider, Content } = Layout;

function App() {
  return (
    <div className="App">
       <Layout>
         <Header>
            <Title style={{color:"white" ,float:"left"}} level={3}>SIDE BAR</Title>
         </Header>
          <Layout>
            <Sider>
               <Menu defaultSelectedKeys={['1']}>
              {/* <Menu.Item key="dashboard">
                
              </Menu.Item> */}
              
              <SubMenu>
                
                <Menu.ItemGroup key="Absolutes" title="About Us">
                  <Menu.Item key="location1">Location 1</Menu.Item>
                  <Menu.Item key="location2">Location 2</Menu.Item>

                </Menu.ItemGroup>
              </SubMenu>
            </Menu>
            </Sider>
           
          <Layout>
              
              <Content style={{ padding: '0 50px' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                  <Breadcrumb.Item>Home</Breadcrumb.Item>
                </Breadcrumb>
                <div style={{background:"#fff",padding:23,minHeight:500}}>Content</div>

                
              </Content>
              <Footer>Footer</Footer>
          </Layout>
          
       </Layout>  
      
    </Layout>
    </div>
  );
}

export default App;
